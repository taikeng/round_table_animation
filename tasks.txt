* init the children list (finished )

* show the children as a circle in page (finished)

* remove child from children list at given position (finished)

	- child should be able to tell it's id and position
	
	- remove the child by given position

* show the blink process in page(finished)

* show the animation of child being removed in page(finished)

* reset the positions after the child is removed as the end of this round(finished)

	- the child next the removed child should have position 0 at the end of the round

* show the blink process in page when given position is greater than the size of children list (finished)

* user could start another round in page(finished)

* show the result of the whole game in page(finished)

* user could input the size of children list and the position that should be removed(finished)

* user can restart the game(finished)

* make the UI looks beautiful(finished)

* record the order of eliminated children(finished)

