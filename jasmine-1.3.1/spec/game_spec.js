describe("Game", function(){
	describe("get result", function(){
		it("should throw exception when game is not finished", function(){
			var game = new Game(10, 3);
			expect(function(){game.result()}).toThrow();
		});	
	});
})