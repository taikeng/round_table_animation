describe("Child", function(){
	var child;
	beforeEach(function(){
		child = new Child(2, 4);
	});

	it("has its id", function(){
		expect(child.id).toEqual(2);
	});

	it("has its position", function(){
		expect(child.position).toEqual(4);
	});

	it("name should be id + 1", function(){
		expect(child.name).toEqual(3);
	})
})