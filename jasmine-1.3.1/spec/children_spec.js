describe("Children", function(){
	var children;

	describe("initialization", function(){
		var size = 10;

		beforeEach(function(){
			children = new Children(size, 8);
		});	

		it("the list should have exact number of children as the given size", function(){
			expect(children.amount()).toEqual(size);
		});
	});

	describe("should get child by index", function(){
		beforeEach(function(){
			children = new Children(10, 2);			
		});
		it("when index < size", function(){
			var child = children.child(7);
			expect(child.id).toEqual(7);
		});
		it("when index = size", function(){
			var child = children.child(10);
			expect(child.id).toEqual(0);
		});
		it("when index > size", function(){
			var child = children.child(15);
			expect(child.id).toEqual(5);
		});
		it("when index = size * n", function(){
			var child = children.child(20);
			expect(child.id).toEqual(0);
		});
		it("when index > size * n", function(){
			var child = children.child(51);
			expect(child.id).toEqual(1);
		});
	});

	describe("remove child from stop position", function(){

		it("the number of children should be decreased", function(){
			children = new Children(5, 2);
			children.removeChildFromStopPosition();
			expect(children.amount()).toEqual(4);
		});

		it("should show the child that is removed", function(){
			children = new Children(5, 3);
			var child = children.removeChildFromStopPosition();
			expect(child.id).toEqual(2);
			expect(child.name).toEqual(3);
		});

		it("should throw exception when try to remove child given there's only one child left", function(){
			children = new Children(4, 3);
			children.removeChildFromStopPosition();
			children.removeChildFromStopPosition();
			children.removeChildFromStopPosition();
			expect(function(){children.removeChildFromStopPosition()}).toThrow();
		});

		it("should assign the children with new position and reorder them", function(){
			children = new Children(5, 2);
			children.removeChildFromStopPosition();

			expect(children.child(0).position).toEqual(1);
			expect(children.child(0).id).toEqual(2);

			expect(children.child(1).position).toEqual(2);
			expect(children.child(1).id).toEqual(3);

			expect(children.child(2).position).toEqual(3);
			expect(children.child(2).id).toEqual(4);

			expect(children.child(3).position).toEqual(4);
			expect(children.child(3).id).toEqual(0);
		});

		it("should assign the children with new position and reorder them when given step length is greater than list size", function(){
			children = new Children(5, 8);
			children.removeChildFromStopPosition();

			expect(children.child(0).position).toEqual(1);
			expect(children.child(0).id).toEqual(3);

			expect(children.child(1).position).toEqual(2);
			expect(children.child(1).id).toEqual(4);

			expect(children.child(2).position).toEqual(3);
			expect(children.child(2).id).toEqual(0);

			expect(children.child(3).position).toEqual(4);
			expect(children.child(3).id).toEqual(1);
		});

	});

	describe("integration test", function(){

		it("remove all children until only one left", function(){
			children = new Children(5, 8);
			expect(children.removeChildFromStopPosition().id).toEqual(2);

			expect(children.child(0).position).toEqual(1);
			expect(children.child(0).id).toEqual(3);

			expect(children.child(1).position).toEqual(2);
			expect(children.child(1).id).toEqual(4);

			expect(children.child(2).position).toEqual(3);
			expect(children.child(2).id).toEqual(0);

			expect(children.child(3).position).toEqual(4);
			expect(children.child(3).id).toEqual(1);


			expect(children.removeChildFromStopPosition().id).toEqual(1);
			expect(children.child(0).position).toEqual(1);
			expect(children.child(0).id).toEqual(3);

			expect(children.child(1).position).toEqual(2);
			expect(children.child(1).id).toEqual(4);

			expect(children.child(2).position).toEqual(3);
			expect(children.child(2).id).toEqual(0);

			expect(children.removeChildFromStopPosition().id).toEqual(4);
			expect(children.child(0).position).toEqual(1);
			expect(children.child(0).id).toEqual(0);

			expect(children.child(1).position).toEqual(2);
			expect(children.child(1).id).toEqual(3);

			expect(children.removeChildFromStopPosition().id).toEqual(3);
			expect(children.child(0).position).toEqual(1);
			expect(children.child(0).id).toEqual(0);
		})
	})
})