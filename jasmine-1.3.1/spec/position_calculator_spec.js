describe("PositionCalculator", function(){
	var positionCalculator;

	describe("should get the stop position", function(){
		it("when step length is less than children amount", function(){
			positionCalculator = new PositionCalculator(8);
			expect(positionCalculator.stopPositionFor(10)).toEqual(8);
		});

		it("when step length equals children amount", function(){
			positionCalculator = new PositionCalculator(10);
			expect(positionCalculator.stopPositionFor(10)).toEqual(10);
		});

		it("when step length is greater than children amount", function(){
			positionCalculator = new PositionCalculator(8);
			expect(positionCalculator.stopPositionFor(5)).toEqual(3);
		});

		it("when step length is twice the children amount", function(){
			positionCalculator = new PositionCalculator(8);
			expect(positionCalculator.stopPositionFor(4)).toEqual(4);
		});	
	});
});