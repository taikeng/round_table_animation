var PositionCalculator = function(stepLength){
	return {
		stopPositionFor: function(amount){
			var result = stepLength % amount;
			if(result === 0){
				result = amount;
			}
			return result; 
		}
	}
}