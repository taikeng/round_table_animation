function Game(size, stepLength){
	var children = new Children(size, stepLength);
	var removedChildren = [];
	return {
		start: function(){
			for (var i = 0; i < children.amount(); i++) {
				var child = children.child(i);
				$(".children").append("<div class='child' id='"+child.id+"'>"+child.name+"</div>")
			};
			$(".children").extremes();
		},
		isFinished: function(){
			return children.amount() === 1;
		},
		result: function(){
			if(!this.isFinished()){
				throw{
					name: "Invalid Method Call", 
					message: "game is not finished yet!"
				}
			}
			return {
				winner: children.child(0).name,
				losers: removedChildren.map(function(item){
					return item.name;
				})
			};
		},
		play: function(callback){
			var self = this;
			var delay = 300;
			for (var i = 0; i < stepLength - 1; i++) {
				window.setTimeout(function(i){
					return function(){
						$("#"+children.child(i).id).css({"background-color": "#FF0000"})
				  			.animate({"background-color": "#FFFFFF"});
					}
				}(i), delay * i);			
			};
			window.setTimeout(function(){
				$("#"+children.child(stepLength - 1).id).animate(
					{opacity: 0}, 0, function(){
						removedChildren.push(children.removeChildFromStopPosition());
						callback();
					}
				);	
			}, delay * stepLength);
		}
	}
}