function Children(size, stepLength){
	var list = [];
	var positionCalculator = new PositionCalculator(stepLength);
	for (var i = 0; i < size; i++) {
		list.push(new Child(i, i+1));
	};

	var recalculateChildrenPositionWithoutChildOn = function(stopPosition){
		var tempArray = [];
		for (var i = stopPosition ; i < list.length; i++) {
			tempArray.push(new Child(list[i].id, list[i].position - stopPosition));
		};
		for (var i = 0; i < stopPosition - 1; i++){
			tempArray.push(new Child(list[i].id, list[i].position + list.length - stopPosition));
		}
		list.splice(0, list.length);
		for (var i = 0; i < tempArray.length; i++) {
			list.push(tempArray[i]);
		};
	}
	
	return {
		child: function(index){
			return list[index % list.length];
		},
		amount: function(){
			return list.length;
		},
		removeChildFromStopPosition: function(){
			if(this.amount() === 1){
				throw{
					name: "Invalid Method Call", 
					message: "There is only one child left, shouldn't try to call this method again."
				}
			}
			var stopPosition = positionCalculator.stopPositionFor(this.amount());
			var childToBeRemoved = this.child(stopPosition - 1);
			recalculateChildrenPositionWithoutChildOn(stopPosition);
			return childToBeRemoved;
		}
	}
}

