var homepage = (function(){
	var game;
	var roundFinishedCallback = function(){
		if(game.isFinished()){
			window.setTimeout(function(){
				showGameResult();
			}, 1000);
		}else{
			$("#play").removeAttr("disabled");
		}
	};

	var showGameResult = function(){
		$("#game_playing").hide();
		$("#play").removeAttr("disabled");
		$("#game_finished").show();
		var result = game.result();
		$("#winner").append("Game finished. Child " + result.winner + " won the game!");
		$("#losers").append("Losers: ");
		for (var i = 0; i < result.losers.length; i++) {
		 	$("#losers").append(result.losers[i] + " ");
		}; 
	};

	var prepareGame = function(){
		for (var i = 2; i < 101; i++) {
			$("#size").append("<option value=" + i + ">" + i + "</option>");
			$("#step_length").append("<option value=" + i + ">" + i + "</option>");
		};
	};

	var startGame = function(){
		$("#game_starting").hide();
		$("#game_playing").show();
		game = new Game($("#size").val(), $("#step_length").val());
		game.start();	
	};

	var restartGame = function(){
		$("#winner").empty();
		$("#game_finished").hide();
		$("#game_starting").show();
		$("#game_playing").hide();
		$(".children").empty();
	};

	var playGame = function(){
		$("#play").attr("disabled", "disabled");
		game.play(roundFinishedCallback);	
	};

	var bindFunction = function(){
		$("#start").click(function(){
			startGame();
		});

		$("#play").click(function(){
			playGame();
		});

		$("#restart").click(function(){
			restartGame();
		});
	};

	return{
		init: function(){
			prepareGame();
			bindFunction();
		}
	}
})();

$(document).ready(function(){
	homepage.init();
});






